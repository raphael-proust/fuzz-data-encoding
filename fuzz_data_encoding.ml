open Crowbar
open Fuzz_data_encoding_ty
open Fuzz_data_encoding_gen
open Fuzz_data_encoding_ops

let value_gen : (value * value option) gen =
  dynamic_bind ty_gen @@ fun (Any ty) ->
  let op = ops ty in
  match op.gen_exn with
  | None -> map [op.gen] (fun v -> (Value (op, v), None))
  | Some gen_exn -> map [op.gen; gen_exn] (fun v w -> (Value (op, v), Some (Value (op, w))))

type 'data encoder = {
  encode : 'a . 'a D.encoding -> 'a -> 'data;
  decode : 'a . 'a D.encoding -> 'data -> 'a;
}

let check_valid encoder (Value (op, v)) =
  let data = encoder.encode op.encoding v in
  match encoder.decode op.encoding data with
  | exception exn ->
     Printf.ksprintf fail
       "Incorrect deserialization (%s)" (Printexc.to_string exn)
  | v' -> check_eq ~pp:op.printer ~eq:op.eq v v'

let check_fail encoder (Value (op, v)) =
  try
    let data = encoder.encode op.encoding v in
    let _v' = encoder.decode op.encoding data in
    Printf.ksprintf fail
      "Failure to fail on invlaid input"
  with
  | exn -> ()

let check encoder (v, w) =
  check_valid encoder v;
  match w with
  | None -> ()
  | Some w -> check_fail encoder w

let json = { encode = D.Json.construct; decode = D.Json.destruct }
let bson = { encode = D.Bson.construct; decode = D.Bson.destruct }
let binary = { encode = D.Binary.to_bytes; decode = D.Binary.of_bytes_exn }

let () =
  add_test ~name:"JSON encoding" [value_gen] (check json);
  add_test ~name:"BSON encoding" [value_gen] (check bson);
  add_test ~name:"Binary encoding" [value_gen] (check binary);
