open Crowbar
open Fuzz_data_encoding_ty
open Fuzz_data_encoding_pp

module Tuple = Fuzz_data_encoding_gen_tuple

(* the sole purpose of this function is to warn when [ty_gen] below
   is not edited after adding a new type representation *)
let generators_for : type a . a ty -> unit = function
  | Int -> ()
  | Int8 -> ()
  | Uint8 -> ()
  | Int16 -> ()
  | Uint16 -> ()
  | Int31 -> ()
  | Int32 -> ()
  | Int64 -> ()
  | Float -> ()
  | Bool -> ()
  | Empty -> ()
  | Null -> ()
  | Constant _ -> ()
  | String -> ()
  | Bytes -> ()
  | Option _ -> ()
  | List _ -> ()
  | Result _ -> ()
  | Tuple _ -> ()
  | Enum _ -> ()
  | Fixed (String _) -> ()
  | Fixed (Bytes _) -> ()
  | Dynamic_size _ -> ()
  | Delayed _ -> ()

let alphanum =
  let of_int i =
    assert (i >= 0 && i < 36);
    if i < 10
    then char_of_int (int_of_char '0' + i)
    else char_of_int (int_of_char 'a' + i - 10)
  in
  let of_int_list li =
    String.init (List.length li) (fun i -> of_int (List.nth li i))
  in
  map [list (range 36)] of_int_list

let set (type a) cmp gen =
  let module S = Set.Make(struct type t = a let compare = cmp end) in
  map [list gen] @@ fun li ->
      List.fold_right S.add li S.empty |> S.elements

let fixed_ty_gen : any_fixed_ty gen =
  let string_length_gen =
    (* Crowbar does not support higher ranges,
       which require too much random bits. *)
    range (1 lsl 7) in
  choose [
    map [string_length_gen] (fun n -> Any_fixed (String n));
    map [string_length_gen] (fun n -> Any_fixed (Bytes n));
  ]

let ty_gen : any_ty gen =
  with_printer (fun ppf (Any ty) -> pp_ty ppf ty) @@
  fix (fun ty_gen -> choose [
    const (Any Int);
    const (Any Int8);
    const (Any Uint8);
    const (Any Int16);
    const (Any Uint16);
    const (Any Int31);
    const (Any Int32);
    const (Any Int64);

    const (Any Float);

    const (Any Bool);

    const (Any Empty);
    const (Any Null);

    (* we have both simple and arbitrary constants *)
    map [alphanum] (fun w -> Any (Constant w));
    map [bytes] (fun s -> Any (Constant s));

    const (Any String);
    const (Any Bytes);

    (* map *)

    map [ty_gen] (fun (Any o) -> Any (Option o));
    map [ty_gen] (fun (Any o) -> Any (List o));
    map [ty_gen; ty_gen] (fun (Any oa) (Any ob) -> Any (Result(oa, ob)));
    map [Tuple.tytup_gen ty_gen] (fun (Any_tup t) -> Any (Tuple t));

    (* string_enum expects at least two keys *)
    map [set String.compare alphanum] (fun keys ->
        guard (List.length keys >= 2);
        let dict = List.map (fun k -> (k, k)) keys in
        Any (Enum(dict)));

    map [fixed_ty_gen] (fun (Any_fixed ft) -> Any (Fixed ft));

    map [ty_gen] (fun (Any t) -> Any (Dynamic_size t));
    map [ty_gen] (fun (Any t) -> Any (Delayed t));
  ])
