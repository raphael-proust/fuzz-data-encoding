open Crowbar
open Fuzz_data_encoding_ty

module Tuple = Fuzz_data_encoding_ops_tuple

module MBytes = Tezos_stdlib__MBytes

let int_ops = {
  ty = Int;
  (* only test 0--2^30 for now *)
  gen = range ~min:0 (1 lsl 30);
  gen_exn = None;
  eq = (=);
  printer = pp_int;
  encoding = D.ranged_int (~-(1 lsl 30)) ((1 lsl 30) - 1);
}

let int8_ops = {
  ty = Int8;
  gen = int8;
  gen_exn = None;
  eq = (=);
  printer = pp_int;
  encoding = D.int8;
}

let uint8_ops = {
  ty = Uint8;
  gen = uint8;
  gen_exn = None;
  eq = (=);
  printer = pp_int;
  encoding = D.uint8;
}

let int16_ops = {
  ty = Int16;
  gen = int16;
  gen_exn = None;
  eq = (=);
  printer = pp_int;
  encoding = D.int16;
}

let uint16_ops = {
  ty = Uint16;
  gen = uint16;
  gen_exn = None;
  eq = (=);
  printer = pp_int;
  encoding = D.uint16;
}

let int31_ops = {
  ty = Int31;
  gen = range ~min:0 (1 lsl 30);
  gen_exn = Some (range ~min:(1 lsl 32) (1 lsl 30));
  eq = (=);
  printer = pp_int;
  encoding = D.int31;
}

let int32_ops = {
  ty = Int32;
  gen = int32;
  gen_exn = None;
  eq = Int32.equal;
  printer = pp_int32;
  encoding = D.int32;
}

let int64_ops = {
  ty = Int64;
  gen = int64;
  gen_exn = None;
  eq = Int64.equal;
  printer = pp_int64;
  encoding = D.int64;
}

(* we ask for bit-precise encoding and decoding of floats *)
let float_ops =
  let printer ppf x = pp ppf "%h" x in
  let eq x y = Int64.(equal (bits_of_float x) (bits_of_float y)) in
  {
    ty = Float;
    gen = with_printer printer float;
    gen_exn = None;
    eq;
    printer;
    encoding = D.float;
  }

let bool_ops = {
  ty = Bool;
  gen = bool;
  gen_exn = None;
  eq = (=);
  printer = pp_bool;
  encoding = D.bool;
}

let null_ops =
  let printer ppf () = pp ppf "null" in
  {
    ty = Null;
    gen = with_printer printer (const ());
    gen_exn = None;
    eq = (=);
    printer;
    encoding = D.null;
  }

let empty_ops =
  let printer ppf () = pp ppf "empty" in
  {
    ty = Empty;
    gen = with_printer printer (const ());
    gen_exn = None;
    eq = (=);
    printer;
    encoding = D.empty;
  }

let constant_ops str =
  let printer ppf () = pp ppf "constant%S" str in
  {
    ty = Constant str;
    gen = with_printer printer (const ());
    gen_exn = None;
    eq = (=);
    printer;
    encoding = D.constant str;
  }

let string_ops =
  let printer ppf str = pp ppf "%S" str in
  {
    ty = String;
    gen = with_printer printer bytes;
    gen_exn = None;
    eq = (=);
    printer;
    encoding = D.string;
  }

let bytes_ops =
  let printer ppf bytes = pp ppf "bytes(%a)" MBytes.pp_hex bytes in
  {
    ty = Bytes;
    gen = with_printer printer (map [bytes] MBytes.of_string);
    gen_exn = None;
    eq = (=);
    printer;
    encoding = D.bytes;
  }

let option_ops o =
  let ty = Option o.ty in
  let gen = option o.gen in
  let gen_exn = match o.gen_exn with
    | None -> None
    | Some gen_exn -> Some (map [gen_exn] (fun e -> Some e))
  in
  let eq m1 m2 = match m1, m2 with
    | None, None -> true
    | None, Some _ | Some _, None -> false
    | Some v1, Some v2 -> o.eq v1 v2 in
  let printer ppf = function
    | None -> pp ppf "None"
    | Some v -> pp ppf "Some(%a)" o.printer v in
  let encoding = D.option o.encoding in
  { ty; gen; gen_exn; eq; printer; encoding }

let list_ops o =
  let ty = List o.ty in
  let gen = list o.gen in
  let gen_exn = match o.gen_exn with
    | None -> None
    | Some gen_exn ->
      let gen_may_exn = choose [o.gen; gen_exn] in
      Some (map
        [list gen_may_exn; gen_exn; list gen_may_exn]
        (fun left e right -> left @ e :: right)
      )
  in
  let eq l1 l2 =
    List.length l1 = List.length l2
    && List.for_all2 o.eq l1 l2 in
  let printer = pp_list o.printer in
  let encoding = D.list o.encoding in
  { ty; gen; gen_exn; eq; printer; encoding }

let result_ops oa ob =
  let ty = Result(oa.ty, ob.ty) in
  let gen = result oa.gen ob.gen in
  let gen_exn = match oa.gen_exn, ob.gen_exn with
    | None, None -> None
    | Some a, None -> Some (map [a] (fun v -> Ok v))
    | None, Some b -> Some (map [b] (fun v -> Error v))
    | Some a, Some b ->
      Some (choose [
          map [a] (fun v -> Ok v);
          map [b] (fun v -> Error v);
        ])
  in
  let eq r1 r2 = match r1, r2 with
    | Ok a1, Ok a2 -> oa.eq a1 a2
    | Ok _, Error _ | Error _, Ok _ -> false
    | Error b1, Error b2 -> ob.eq b1 b2 in
  let printer ppf = function
    | Ok va -> pp ppf "Ok(%a)" oa.printer va
    | Error vb -> pp ppf "Error(%a)" ob.printer vb in
  let encoding = D.result oa.encoding ob.encoding in
  { ty; gen; gen_exn; eq; printer; encoding }

let enum_ops (type a) (dict : (string * a) list) =
  let ty = Enum dict in
  let gen = choose (List.map (fun (k, v) -> const v) dict) in
  let gen_exn = None in
  let pair v0 = List.find_opt (fun (k, v) -> v = v0) dict in
  let eq v1 v2 =
    match pair v1, pair v2 with
    | Some (k1, _), Some (k2, _) -> String.equal k1 k2
    | _, _ -> false
  in
  let printer ppf v =
    match pair v with
    | None -> pp ppf "?"
    | Some (k, v) -> pp ppf "key:%S" k
  in
  let encoding = D.string_enum dict in
  { ty; gen; gen_exn; eq; printer; encoding }

let fixed_string_ops n =
  let ty = Fixed (String n) in
  let gen = bytes_fixed n in
  let gen_exn : string gen option =
    if n > 0 then
      Some (choose [
        dynamic_bind (range n) bytes_fixed;
        dynamic_bind (range n) (fun m -> bytes_fixed (n + m + 1))
      ])
    else
        Some (dynamic_bind (range n) (fun m -> bytes_fixed (n + m + 1)))
  in
  let eq = String.equal in
  let printer = pp_string in
  let encoding = D.Fixed.string n in
  { ty; gen; gen_exn; eq; printer; encoding }

let fixed_bytes_ops n =
  let ty = Fixed (Bytes n) in
  let printer = bytes_ops.printer in
  let gen = with_printer printer (map [bytes_fixed n] MBytes.of_string) in
  let gen_exn =
    if n > 0 then
      Some (map [
        choose [
          dynamic_bind (range n) bytes_fixed;
          dynamic_bind (range n) (fun m -> bytes_fixed (n + m + 1));
        ]
      ] MBytes.of_string)
    else
      Some (map [
        dynamic_bind (range n) (fun m -> bytes_fixed (n + m + 1))
      ] MBytes.of_string)
  in
  let eq = bytes_ops.eq in
  let encoding = D.Fixed.bytes n in
  { ty; gen; gen_exn; eq; printer; encoding }

let dynamic_size_ops o =
  let ty = Dynamic_size o.ty in
  let encoding = D.dynamic_size o.encoding in
  { o with ty; encoding; }

let delayed_ops o =
  let ty = Delayed o.ty in
  let encoding = D.delayed (fun () -> o.encoding) in
  { o with ty; encoding; }

let rec ops : type a . a ty -> a ops = function
  | Int -> int_ops
  | Int8 -> int8_ops
  | Uint8 -> uint8_ops
  | Int16 -> int16_ops
  | Uint16 -> uint16_ops
  | Int31 -> int31_ops
  | Int32 -> int32_ops
  | Int64 -> int64_ops
  | Float -> float_ops
  | Bool -> bool_ops
  | Null -> null_ops
  | Empty -> empty_ops
  | Constant str -> constant_ops str
  | String -> string_ops
  | Bytes -> bytes_ops
  | Option t -> option_ops (ops t)
  | List t -> list_ops (ops t)
  | Result(ta, tb) -> result_ops (ops ta) (ops tb)
  | Tuple tutyp -> Tuple.(tutyp_ops {ops}) tutyp
  | Enum dict -> enum_ops dict
  | Fixed (String n) -> fixed_string_ops n
  | Fixed (Bytes n) -> fixed_bytes_ops n
  | Dynamic_size t -> dynamic_size_ops (ops t)
  | Delayed t -> delayed_ops (ops t)
