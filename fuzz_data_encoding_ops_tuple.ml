open Crowbar
open Fuzz_data_encoding_ty

let tup1_ops o1 =
  let ty = Tuple(T1(o1.ty)) in
  let gen = map [o1.gen]
              (fun v1 ->
                (v1)) in
  let gen_exn = match o1.gen_exn with
    | Some g1 -> Some (map [g1] (fun v1 -> (v1)))
    | _ -> None in
  let eq
        (a1)
        (b1)
    =
    o1.eq a1 b1
  in
  let printer ppf (v1) =
    Format.fprintf ppf "T1(%a)"
      o1.printer v1
  in
  let encoding =
    D.tup1
      o1.encoding
  in
  { ty; gen; gen_exn; eq; printer; encoding }

let tup2_ops o1 o2 =
  let ty = Tuple(T2(o1.ty, o2.ty)) in
  let gen = map [o1.gen; o2.gen]
              (fun v1 v2 ->
                (v1, v2)) in
  let gen_exn = match o1.gen_exn, o2.gen_exn with
    | None, None -> None
    | Some g1, None -> Some (map [g1; o2.gen] (fun v1 v2 -> (v1, v2)))
    | None, Some g2 -> Some (map [o1.gen; g2] (fun v1 v2 -> (v1, v2)))
    | Some g1, Some g2 ->
      Some (choose [
        map [g1; o2.gen] (fun v1 v2 -> (v1, v2));
        map [o1.gen; g2] (fun v1 v2 -> (v1, v2));
        map [g1; g2] (fun v1 v2 -> (v1, v2));
      ])
    in
  let eq
        (a1, a2)
        (b1, b2)
    =
    o1.eq a1 b1
    && o2.eq a2 b2
  in
  let printer ppf (v1, v2) =
    Format.fprintf ppf "T2(%a, %a)"
      o1.printer v1 o2.printer v2
  in
  let encoding =
    D.tup2
      o1.encoding o2.encoding
  in
  { ty; gen; gen_exn; eq; printer; encoding }

let tup3_ops o1 o2 o3 =
  let ty = Tuple(T3(o1.ty, o2.ty, o3.ty)) in
  let gen = map [o1.gen; o2.gen; o3.gen]
              (fun v1 v2 v3 ->
                (v1, v2, v3)) in
  let gen_exn = match o1.gen_exn, o2.gen_exn, o3.gen_exn with
    | Some g1, Some g2, Some g3 ->
      Some (map [g1; g2; g3] (fun v1 v2 v3 -> (v1, v2, v3)))
    | _ -> None in
  let eq
        (a1, a2, a3)
        (b1, b2, b3)
    =
    o1.eq a1 b1
    && o2.eq a2 b2
    && o3.eq a3 b3
  in
  let printer ppf (v1, v2, v3) =
    Format.fprintf ppf "T3(%a, %a, %a)"
      o1.printer v1 o2.printer v2 o3.printer v3
  in
  let encoding =
    D.tup3
      o1.encoding o2.encoding o3.encoding
  in
  { ty; gen; gen_exn; eq; printer; encoding }

let tup4_ops o1 o2 o3 o4 =
  let ty = Tuple(T4(o1.ty, o2.ty, o3.ty, o4.ty)) in
  let gen = map [o1.gen; o2.gen; o3.gen; o4.gen]
              (fun v1 v2 v3 v4 ->
                (v1, v2, v3, v4)) in
  let gen_exn = match o1.gen_exn, o2.gen_exn, o3.gen_exn, o4.gen_exn with
    | Some g1, Some g2, Some g3, Some g4 ->
      Some (map [g1; g2; g3; g4] (fun v1 v2 v3 v4 -> (v1, v2, v3, v4)))
    | _ -> None in
  let eq
        (a1, a2, a3, a4)
        (b1, b2, b3, b4)
    =
    o1.eq a1 b1
    && o2.eq a2 b2
    && o3.eq a3 b3
    && o4.eq a4 b4
  in
  let printer ppf (v1, v2, v3, v4) =
    Format.fprintf ppf "T4(%a, %a, %a, %a)"
      o1.printer v1 o2.printer v2 o3.printer v3 o4.printer v4
  in
  let encoding =
    D.tup4
      o1.encoding o2.encoding o3.encoding o4.encoding
  in
  { ty; gen; gen_exn; eq; printer; encoding }

let tup5_ops o1 o2 o3 o4 o5 =
  let ty = Tuple(T5(o1.ty, o2.ty, o3.ty, o4.ty, o5.ty)) in
  let gen = map [o1.gen; o2.gen; o3.gen; o4.gen; o5.gen]
              (fun v1 v2 v3 v4 v5 ->
                (v1, v2, v3, v4, v5)) in
  let gen_exn = None in
  let eq
        (a1, a2, a3, a4, a5)
        (b1, b2, b3, b4, b5)
    =
    o1.eq a1 b1
    && o2.eq a2 b2
    && o3.eq a3 b3
    && o4.eq a4 b4
    && o5.eq a5 b5
  in
  let printer ppf (v1, v2, v3, v4, v5) =
    Format.fprintf ppf "T5(%a, %a, %a, %a, %a)"
      o1.printer v1 o2.printer v2 o3.printer v3 o4.printer v4 o5.printer v5
  in
  let encoding =
    D.tup5
      o1.encoding o2.encoding o3.encoding o4.encoding o5.encoding
  in
  { ty; gen; gen_exn; eq; printer; encoding }

let tup6_ops o1 o2 o3 o4 o5 o6 =
  let ty = Tuple(T6(o1.ty, o2.ty, o3.ty, o4.ty, o5.ty,
                     o6.ty)) in
  let gen = map [o1.gen; o2.gen; o3.gen; o4.gen; o5.gen;
                 o6.gen]
              (fun v1 v2 v3 v4 v5 v6 ->
                (v1, v2, v3, v4, v5, v6)) in
  let gen_exn = None in
  let eq
        (a1, a2, a3, a4, a5, a6)
        (b1, b2, b3, b4, b5, b6)
    =
    o1.eq a1 b1
    && o2.eq a2 b2
    && o3.eq a3 b3
    && o4.eq a4 b4
    && o5.eq a5 b5
    && o6.eq a6 b6
  in
  let printer ppf (v1, v2, v3, v4, v5, v6) =
    Format.fprintf ppf "T6(%a, %a, %a, %a, %a, %a)"
      o1.printer v1 o2.printer v2 o3.printer v3 o4.printer v4 o5.printer v5
      o6.printer v6
  in
  let encoding =
    D.tup6
      o1.encoding o2.encoding o3.encoding o4.encoding o5.encoding
      o6.encoding
  in
  { ty; gen; gen_exn; eq; printer; encoding }

let tup7_ops o1 o2 o3 o4 o5 o6 o7 =
  let ty = Tuple(T7(o1.ty, o2.ty, o3.ty, o4.ty, o5.ty,
                     o6.ty, o7.ty)) in
  let gen = map [o1.gen; o2.gen; o3.gen; o4.gen; o5.gen;
                 o6.gen; o7.gen]
              (fun v1 v2 v3 v4 v5 v6 v7 ->
                (v1, v2, v3, v4, v5, v6, v7)) in
  let gen_exn = None in
  let eq
        (a1, a2, a3, a4, a5, a6, a7)
        (b1, b2, b3, b4, b5, b6, b7)
    =
    o1.eq a1 b1
    && o2.eq a2 b2
    && o3.eq a3 b3
    && o4.eq a4 b4
    && o5.eq a5 b5
    && o6.eq a6 b6
    && o7.eq a7 b7
  in
  let printer ppf (v1, v2, v3, v4, v5, v6, v7) =
    Format.fprintf ppf "T7(%a, %a, %a, %a, %a, %a, %a)"
      o1.printer v1 o2.printer v2 o3.printer v3 o4.printer v4 o5.printer v5
      o6.printer v6 o7.printer v7
  in
  let encoding =
    D.tup7
      o1.encoding o2.encoding o3.encoding o4.encoding o5.encoding
      o6.encoding o7.encoding
  in
  { ty; gen; gen_exn; eq; printer; encoding }

let tup8_ops o1 o2 o3 o4 o5 o6 o7 o8 =
  let ty = Tuple(T8(o1.ty, o2.ty, o3.ty, o4.ty, o5.ty,
                     o6.ty, o7.ty, o8.ty)) in
  let gen = map [o1.gen; o2.gen; o3.gen; o4.gen; o5.gen;
                 o6.gen; o7.gen; o8.gen]
              (fun v1 v2 v3 v4 v5 v6 v7 v8 ->
                (v1, v2, v3, v4, v5, v6, v7, v8)) in
  let gen_exn = None in
  let eq
        (a1, a2, a3, a4, a5, a6, a7, a8)
        (b1, b2, b3, b4, b5, b6, b7, b8)
    =
    o1.eq a1 b1
    && o2.eq a2 b2
    && o3.eq a3 b3
    && o4.eq a4 b4
    && o5.eq a5 b5
    && o6.eq a6 b6
    && o7.eq a7 b7
    && o8.eq a8 b8
  in
  let printer ppf (v1, v2, v3, v4, v5, v6, v7, v8) =
    Format.fprintf ppf "T8(%a, %a, %a, %a, %a, %a, %a, %a)"
      o1.printer v1 o2.printer v2 o3.printer v3 o4.printer v4 o5.printer v5
      o6.printer v6 o7.printer v7 o8.printer v8
  in
  let encoding =
    D.tup8
      o1.encoding o2.encoding o3.encoding o4.encoding o5.encoding
      o6.encoding o7.encoding o8.encoding
  in
  { ty; gen; gen_exn; eq; printer; encoding }

let tup9_ops o1 o2 o3 o4 o5 o6 o7 o8 o9 =
  let ty = Tuple(T9(o1.ty, o2.ty, o3.ty, o4.ty, o5.ty,
                     o6.ty, o7.ty, o8.ty, o9.ty)) in
  let gen = map [o1.gen; o2.gen; o3.gen; o4.gen; o5.gen;
                 o6.gen; o7.gen; o8.gen; o9.gen]
              (fun v1 v2 v3 v4 v5 v6 v7 v8 v9 ->
                (v1, v2, v3, v4, v5, v6, v7, v8, v9)) in
  let gen_exn = None in
  let eq
        (a1, a2, a3, a4, a5, a6, a7, a8, a9)
        (b1, b2, b3, b4, b5, b6, b7, b8, b9)
    =
    o1.eq a1 b1
    && o2.eq a2 b2
    && o3.eq a3 b3
    && o4.eq a4 b4
    && o5.eq a5 b5
    && o6.eq a6 b6
    && o7.eq a7 b7
    && o8.eq a8 b8
    && o9.eq a9 b9
  in
  let printer ppf (v1, v2, v3, v4, v5, v6, v7, v8, v9) =
    Format.fprintf ppf "T9(%a, %a, %a, %a, %a, %a, %a, %a, %a)"
      o1.printer v1 o2.printer v2 o3.printer v3 o4.printer v4 o5.printer v5
      o6.printer v6 o7.printer v7 o8.printer v8 o9.printer v9
  in
  let encoding =
    D.tup9
      o1.encoding o2.encoding o3.encoding o4.encoding o5.encoding
      o6.encoding o7.encoding o8.encoding o9.encoding
  in
  { ty; gen; gen_exn; eq; printer; encoding }

let tup10_ops o1 o2 o3 o4 o5 o6 o7 o8 o9 o10 =
  let ty = Tuple(T10(o1.ty, o2.ty, o3.ty, o4.ty, o5.ty,
                     o6.ty, o7.ty, o8.ty, o9.ty, o10.ty)) in
  let gen = map [o1.gen; o2.gen; o3.gen; o4.gen; o5.gen;
                 o6.gen; o7.gen; o8.gen; o9.gen; o10.gen]
              (fun v1 v2 v3 v4 v5 v6 v7 v8 v9 v10 ->
                (v1, v2, v3, v4, v5, v6, v7, v8, v9, v10)) in
  let gen_exn = None in
  let eq
        (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10)
        (b1, b2, b3, b4, b5, b6, b7, b8, b9, b10)
    =
    o1.eq a1 b1
    && o2.eq a2 b2
    && o3.eq a3 b3
    && o4.eq a4 b4
    && o5.eq a5 b5
    && o6.eq a6 b6
    && o7.eq a7 b7
    && o8.eq a8 b8
    && o9.eq a9 b9
    && o10.eq a10 b10
  in
  let printer ppf (v1, v2, v3, v4, v5, v6, v7, v8, v9, v10) =
    Format.fprintf ppf "T10(%a, %a, %a, %a, %a, %a, %a, %a, %a, %a)"
      o1.printer v1 o2.printer v2 o3.printer v3 o4.printer v4 o5.printer v5
      o6.printer v6 o7.printer v7 o8.printer v8 o9.printer v9 o10.printer v10
  in
  let encoding =
    D.tup10
      o1.encoding o2.encoding o3.encoding o4.encoding o5.encoding
      o6.encoding o7.encoding o8.encoding o9.encoding o10.encoding
  in
  { ty; gen; gen_exn; eq; printer; encoding }

type poly_ops = { ops : 'a . 'a ty -> 'a ops }

let rec tutyp_ops : type a . poly_ops -> a tytup -> a ops =
  fun {ops} -> function
  | T1(t1) -> tup1_ops (ops t1)
  | T2(t1, t2) ->
     tup2_ops
       (ops t1) (ops t2)
  | T3(t1, t2, t3) ->
     tup3_ops
       (ops t1) (ops t2) (ops t3)
  | T4(t1, t2, t3, t4) ->
     tup4_ops
       (ops t1) (ops t2) (ops t3) (ops t4)
  | T5(t1, t2, t3, t4, t5) ->
     tup5_ops
       (ops t1) (ops t2) (ops t3) (ops t4) (ops t5)
  | T6(t1, t2, t3, t4, t5,
        t6) ->
     tup6_ops
       (ops t1) (ops t2) (ops t3) (ops t4) (ops t5)
       (ops t6)
  | T7(t1, t2, t3, t4, t5,
        t6, t7) ->
     tup7_ops
       (ops t1) (ops t2) (ops t3) (ops t4) (ops t5)
       (ops t6) (ops t7)
  | T8(t1, t2, t3, t4, t5,
        t6, t7, t8) ->
     tup8_ops
       (ops t1) (ops t2) (ops t3) (ops t4) (ops t5)
       (ops t6) (ops t7) (ops t8)
  | T9(t1, t2, t3, t4, t5,
        t6, t7, t8, t9) ->
     tup9_ops
       (ops t1) (ops t2) (ops t3) (ops t4) (ops t5)
       (ops t6) (ops t7) (ops t8) (ops t9)
  | T10(t1, t2, t3, t4, t5,
        t6, t7, t8, t9, t10) ->
     tup10_ops
       (ops t1) (ops t2) (ops t3) (ops t4) (ops t5)
       (ops t6) (ops t7) (ops t8) (ops t9) (ops t10)
