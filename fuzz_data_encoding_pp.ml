open Fuzz_data_encoding_ty
module Tuple = Fuzz_data_encoding_pp_tuple

let pp_dict ppf dict =
  let pp_pair (key, _v) = Format.fprintf ppf "%S; " key in
  List.iter pp_pair dict

let rec pp_ty : type a . a ty printer = fun ppf ->
  let printf fmt = Format.fprintf ppf fmt in
  function
  | Int -> printf "Int"
  | Int8 -> printf "Int8"
  | Uint8 -> printf "Uint8"
  | Int16 -> printf "Int16"
  | Uint16 -> printf "Uint16"
  | Int31 -> printf "Int31"
  | Int32 -> printf "Int32"
  | Int64 -> printf "Int64"
  | Float -> printf "Float"
  | Bool -> printf "Bool"
  | Null -> printf "Null"
  | Empty -> printf "Empty"
  | String -> printf "String"
  | Bytes -> printf "Bytes"
  | Constant str -> printf "Constant%S" str
  | Option t -> printf "Option(%a)" pp_ty t
  | List t -> printf "List(%a)" pp_ty t
  | Enum dict -> printf "Enum[%a]" pp_dict dict
  | Result(ta, tb) -> printf "Result(%a, %a)" pp_ty ta pp_ty tb
  | Tuple tup -> printf "Tuple(%a)" Tuple.(pp_tuple {pp_ty}) tup
  | Fixed ft -> printf "Fixed(%a)" pp_fixed_ty ft
  | Dynamic_size t -> printf "Dynamic_size(%a)" pp_ty t
  | Delayed t -> printf "Delayed(%a)" pp_ty t

and pp_fixed_ty : type a . a fixed_ty printer = fun ppf ->
  let printf fmt = Format.fprintf ppf fmt in
  function
  | String n -> printf "String(%d)" n
  | Bytes n -> printf "Bytes(%d)" n
