open Fuzz_data_encoding_ty

type poly_pp_ty = {pp_ty : 'a . 'a ty printer}

let pp_tuple pp_ty (type a) : a tytup printer = fun ppf ->
  let printf fmt = Format.fprintf ppf fmt in
  let p = pp_ty.pp_ty in
  function
  | T1(t1) ->
     printf "T1(%a)"
       p t1
  | T2(t1, t2) ->
     printf "T2(%a, %a)"
       p t1 p t2
  | T3(t1, t2, t3) ->
     printf "T3(%a, %a, %a)"
       p t1 p t2 p t3
  | T4(t1, t2, t3, t4) ->
     printf "T4(%a, %a, %a, %a)"
       p t1 p t2 p t3 p t4
  | T5(t1, t2, t3, t4, t5) ->
     printf "T5(%a, %a, %a, %a, %a)"
       p t1 p t2 p t3 p t4 p t5
  | T6(t1, t2, t3, t4, t5, t6) ->
     printf "T6(%a, %a, %a, %a, %a, %a)"
       p t1 p t2 p t3 p t4 p t5 p t6
  | T7(t1, t2, t3, t4, t5, t6, t7) ->
     printf "T7(%a, %a, %a, %a, %a, %a, %a)"
       p t1 p t2 p t3 p t4 p t5 p t6 p t7
  | T8(t1, t2, t3, t4, t5, t6, t7, t8) ->
     printf "T8(%a, %a, %a, %a, %a, %a, %a, %a)"
       p t1 p t2 p t3 p t4 p t5 p t6 p t7 p t8
  | T9(t1, t2, t3, t4, t5, t6, t7, t8, t9) ->
     printf "T9(%a, %a, %a, %a, %a, %a, %a, %a, %a)"
       p t1 p t2 p t3 p t4 p t5 p t6 p t7 p t8 p t9
  | T10(t1, t2, t3, t4, t5, t6, t7, t8, t9, t10) ->
     printf "T10(%a, %a, %a, %a, %a, %a, %a, %a, %a, %a)"
       p t1 p t2 p t3 p t4 p t5 p t6 p t7 p t8 p t9 p t10
