open Crowbar
module D = Tezos_data_encoding.Data_encoding
module Tuple = Fuzz_data_encoding_tuple

module MBytes = Tezos_stdlib__MBytes

[@@@warning "-30"];;

type _ ty =
  | Int : int ty
  | Int8 : int ty
  | Uint8 : int ty
  | Int16 : int ty
  | Uint16 : int ty
  | Int31 : int ty
  | Int32 : int32 ty
  | Int64 : int64 ty
  (* ranged_int? *)
  (* ranged_float? *)
  | Float : float ty
  | Bool : bool ty
  | Null : unit ty
  | Empty : unit ty
  | Constant : string -> unit ty
  | String : string ty
  | Bytes : MBytes.t ty
  | Option : 'a ty -> 'a option ty
  | List : 'a ty -> 'a list ty
  | Result : 'a ty * 'b ty -> ('a, 'b) result ty
  | Tuple : 'a tytup -> 'a ty
  | Enum : (string * 'a) list -> 'a ty
  | Fixed : 'a fixed_ty -> 'a ty
  | Dynamic_size : 'a ty -> 'a ty
  | Delayed : 'a ty -> 'a ty

and _ fixed_ty =
  | String : int -> string fixed_ty
  | Bytes : int -> MBytes.t fixed_ty

and _ tytup =
  | T1  : 't1 ty
            -> 't1 tytup
  | T2  : 't1 ty * 't2 ty
            -> ('t1 * 't2) tytup
  | T3  : 't1 ty * 't2 ty * 't3 ty
            -> ('t1 * 't2 * 't3) tytup
  | T4  : 't1 ty * 't2 ty * 't3 ty * 't4 ty
            -> ('t1 * 't2 * 't3 * 't4) tytup
  | T5  : 't1 ty * 't2 ty * 't3 ty * 't4 ty * 't5 ty
            -> ('t1 * 't2 * 't3 * 't4* 't5) tytup
  | T6  : 't1 ty * 't2 ty * 't3 ty * 't4 ty * 't5 ty* 't6 ty
            -> ('t1 * 't2 * 't3 * 't4* 't5 * 't6) tytup
  | T7  : 't1 ty * 't2 ty * 't3 ty * 't4 ty * 't5 ty * 't6 ty * 't7 ty
            -> ('t1 * 't2 * 't3 * 't4* 't5 * 't6 * 't7) tytup
  | T8  : 't1 ty * 't2 ty * 't3 ty * 't4 ty * 't5 ty * 't6 ty * 't7 ty * 't8 ty
            -> ('t1 * 't2 * 't3 * 't4* 't5 * 't6 * 't7 * 't8) tytup
  | T9  : 't1 ty * 't2 ty * 't3 ty * 't4 ty * 't5 ty * 't6 ty * 't7 ty * 't8 ty * 't9 ty
            -> ('t1 * 't2 * 't3 * 't4* 't5 * 't6 * 't7 * 't8 * 't9) tytup
  | T10 : 't1 ty * 't2 ty * 't3 ty * 't4 ty * 't5 ty * 't6 ty * 't7 ty * 't8 ty * 't9 ty * 't10 ty
            -> ('t1 * 't2 * 't3 * 't4* 't5 * 't6 * 't7 * 't8 * 't9 * 't10) tytup

type any_ty = Any : 'a ty -> any_ty

type any_fixed_ty = Any_fixed : 'a fixed_ty -> any_fixed_ty

type any_tytup = Any_tup : 'a tytup -> any_tytup

type 'a eq = 'a -> 'a -> bool
type 'a printer = 'a Crowbar.printer

type 'a ops = {
  ty : 'a ty;
  eq : 'a eq;
  printer : 'a printer;
  gen : 'a gen;
  gen_exn: 'a gen option; (* A generator for invlaid values *)
  encoding : 'a D.encoding;
}

type value = Value : 'a ops * 'a -> value

