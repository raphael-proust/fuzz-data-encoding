(* This file is design to be self-contained and
   provide an executable summary of the test failures
   found at a given time. As we report the issues we found
   by testing, failure cases may get fixed and eventually
   get removed from here.
*)
module D = Tezos_data_encoding.Data_encoding

module MBytes = Tezos_stdlib.MBytes

type 'data encoder = {
  name : string;
  encode : 'a . 'a D.encoding -> 'a -> 'data;
  decode : 'a . 'a D.encoding -> 'data -> 'a;
}

let json =
  let open D.Json in
  { name = "json"; encode = construct; decode = destruct }
let bson =
  let open D.Bson in
  { name = "bson"; encode = construct; decode = destruct }
let binary =
  let open D.Binary in
  { name = "binary"; encode = to_bytes; decode = of_bytes_exn }

let roundtrip encoder encoding eq pp value =
  let printf fmt = Printf.fprintf stdout fmt in
  printf "Encoding %a in %s...%!" pp value encoder.name;
  let data = encoder.encode encoding value in
  begin match encoder.decode encoding data with
  | exception exn ->
      printf "WRONG: decoding exception %s" (Printexc.to_string exn)
  | result ->
      if eq value result
      then printf "ok."
      else printf "WRONG: different decoding %a" pp result
  end;
  printf "\n%!"

let printer fmt = fun ppf -> Printf.fprintf ppf fmt

let list_printer printer = fun ppf li ->
  Printf.fprintf ppf "[";
  List.iter (fun v -> Printf.fprintf ppf "%a;" printer v) li;
  Printf.fprintf ppf "]";
  ()

let int_printer = printer "%d"
let bool_printer = printer "%b"
let string_printer = printer "%S"
let bytes_printer ppf bytes =
  Printf.fprintf ppf "bytes(%S)" (MBytes.to_string bytes)

let empty_printer ppf () = Printf.fprintf ppf "empty"
let null_printer ppf () = Printf.fprintf ppf "null"
let constant_printer str ppf () = Printf.fprintf ppf "constant%S" str

let option_printer printer = fun ppf -> function
  | None -> Printf.fprintf ppf "None"
  | Some v -> Printf.fprintf ppf "Some(%a)" printer v

let result_printer pa pb = fun ppf -> function
  | Ok a -> Printf.fprintf ppf "Ok(%a)" pa a
  | Error b -> Printf.fprintf ppf "Error(%a)" pb b

let int = D.ranged_int (~-(1 lsl 30)) ((1 lsl 30) - 1)

let () =
  begin
    print_endline "Failure on lists containing fixed-sized elements \
                   that are empty when serialized (only in binary)";
    let test enc v printer =
      roundtrip binary
        (D.list enc) (=)
        (list_printer printer)
        [v] (* => [] *)
    in
    test (D.Fixed.string 0) "" string_printer;
    test (D.Fixed.bytes 0) (MBytes.of_string "") bytes_printer;
    print_newline ();
  end;
